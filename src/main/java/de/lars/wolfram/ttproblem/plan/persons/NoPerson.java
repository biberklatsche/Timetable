package de.lars.wolfram.ttproblem.plan.persons;

import java.time.LocalTime;

/**
 * @author wolframl
 */
public class NoPerson implements Person
{
    private static final String name = "/";

    @Override public LocalTime getEndWorking()
    {
        return LocalTime.MAX;
    }

    @Override public String getName()
    {
        return name;
    }

    @Override public LocalTime getStartWorking()
    {
        return LocalTime.MIN;
    }
}
