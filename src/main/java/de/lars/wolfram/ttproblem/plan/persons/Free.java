package de.lars.wolfram.ttproblem.plan.persons;

import java.time.LocalTime;

/**
 * @author wolframl
 */
@FixedPerson public class Free implements Person
{
    @Override public LocalTime getEndWorking()
    {
        return LocalTime.MAX;
    }

    @Override public String getName()
    {
        return "";
    }

    @Override public LocalTime getStartWorking()
    {
        return LocalTime.MIN;
    }
}
