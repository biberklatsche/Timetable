package de.lars.wolfram.ttproblem.plan.persons;

import java.time.LocalTime;

/**
 * @author wolframl
 */
public class NaturalPerson implements Person
{
    private final String    name;
    private final LocalTime startWorking;
    private final LocalTime endWorking;

    public NaturalPerson(String name, LocalTime startWorking, LocalTime endWorking)
    {
        this.name = name;
        this.startWorking = startWorking;
        this.endWorking = endWorking;
    }

    public LocalTime getEndWorking()
    {
        return endWorking;
    }

    @Override public String getName()
    {
        return name;
    }

    public LocalTime getStartWorking()
    {
        return startWorking;
    }

    @Override public String toString()
    {
        return name + " " + getStartWorking().toString() + "-" + getEndWorking().toString();
    }
}
