package de.lars.wolfram.ttproblem.plan.persons;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wolframl
 */
@Retention(RetentionPolicy.RUNTIME) @Target(ElementType.TYPE) public @interface FixedPerson
{
}
