package de.lars.wolfram.ttproblem.plan.time;

import java.time.LocalTime;

/**
 * @author wolframl
 */
public interface TimeSlot
{
    LocalTime getFrom();

    LocalTime getTo();
}
