package de.lars.wolfram.ttproblem.plan.timetable;

import java.util.Arrays;
import java.util.List;

/**
 * @author wolframl
 */
public class Timetable
{

    private Morning morning;

    public Timetable(Morning morning)
    {
        this.morning = morning;
    }

    public List<TimetablePart> getParts()
    {
        return Arrays.asList(morning);
    }

    @Override public String toString()
    {
        return morning.toString();
    }

}
