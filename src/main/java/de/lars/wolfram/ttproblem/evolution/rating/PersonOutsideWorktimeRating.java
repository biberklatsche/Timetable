package de.lars.wolfram.ttproblem.evolution.rating;

import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;
import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;
import de.lars.wolfram.ttproblem.plan.timetable.TimetableKey;

/**
 * Created by lwo on 02.05.2016.
 */
public class PersonOutsideWorktimeRating implements Rater
{

    private int points = 1;

    @Override public Double rate(IndividualPart timetable)
    {
        Double rating = 0.0;
        for (Room room : timetable.getRooms())
        {
            for (TimeSlot timeSlot : timetable.getTimeSlots())
            {
                Person person = timetable.getPerson(new TimetableKey(timeSlot, room));
                if (isPersonOutsideSlot(person, timeSlot))
                {
                    rating += points;
                }
            }

        }
        return rating;
    }

    private boolean isPersonOutsideSlot(Person person, TimeSlot timeSlot)
    {
        return timeSlot.getFrom().isBefore(person.getStartWorking()) || timeSlot.getTo()
                .isAfter(person.getEndWorking());
    }
}
