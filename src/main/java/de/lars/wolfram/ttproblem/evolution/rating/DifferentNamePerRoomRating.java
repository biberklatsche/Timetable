package de.lars.wolfram.ttproblem.evolution.rating;

import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;
import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.Person;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wolframl
 */
public class DifferentNamePerRoomRating implements Rater
{

    public Double rate(IndividualPart individualPart)
    {
        Double rating = 0.0;
        for (Room room : individualPart.getRooms())
        {
            Map<Person, Integer> namesCount = new HashMap<>();
            for (Person person : individualPart.getPersons(room))
            {
                if (namesCount.containsKey(person))
                {
                    namesCount.put(person, namesCount.get(person) + 1);
                }
                else
                {
                    namesCount.put(person, 1);
                }

            }
            int max = namesCount.values().stream().max(Integer::max).get();
            rating += individualPart.countTimeSlots() - max;
        }
        return rating;
    }
}
