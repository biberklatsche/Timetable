package de.lars.wolfram.ttproblem.evolution.individual;

import de.lars.wolfram.ttproblem.evolution.rand.Randomizer;
import de.lars.wolfram.ttproblem.evolution.rating.DifferentNamePerRoomRating;
import de.lars.wolfram.ttproblem.evolution.rating.PersonInDifferentRoomsPerSlotRating;
import de.lars.wolfram.ttproblem.evolution.rating.PersonOutsideWorktimeRating;
import de.lars.wolfram.ttproblem.evolution.rating.Rater;
import de.lars.wolfram.ttproblem.plan.timetable.Timetable;
import de.lars.wolfram.ttproblem.plan.timetable.TimetableKey;
import de.lars.wolfram.ttproblem.plan.timetable.TimetablePart;
import de.lars.wolfram.ttproblem.plan.timetable.TimetablePartBuilder;

import java.util.Arrays;
import java.util.List;

/**
 * @author wolframl
 */
public class IndividualPartBuilder
{
    private final Randomizer           randomGenerator;
    private       TimetablePartBuilder timetableBuilder;

    private IndividualPartBuilder(Randomizer randomGenerator)
    {
        this.randomGenerator = randomGenerator;
    }

    public IndividualPart build()
    {
        Timetable timetable = timetableBuilder.build();
        fillTimetable(timetable);
        List<Rater> raters = Arrays
                .asList(new DifferentNamePerRoomRating(), new PersonInDifferentRoomsPerSlotRating(),
                        new PersonOutsideWorktimeRating());
        IndividualPart individualPart = new IndividualPart(raters);
        individualPart.setTimetablePart(timetable.getParts().get(0));
        individualPart.rate();
        return individualPart;
    }

    public IndividualPartBuilder times(TimetablePartBuilder timetableBuilder)
    {
        this.timetableBuilder = timetableBuilder;
        return this;
    }

    private void fillTimetable(Timetable timetable)
    {
        for (TimetablePart part : timetable.getParts())
        {
            for (TimetableKey key : part.getReplaceableKeys())
            {
                part.setPerson(key, randomGenerator.anyPerson());
            }
        }
    }

    public static IndividualPartBuilder newBuilder(Randomizer randomGenerator)
    {
        IndividualPartBuilder builder = new IndividualPartBuilder(randomGenerator);
        return builder;
    }
}
