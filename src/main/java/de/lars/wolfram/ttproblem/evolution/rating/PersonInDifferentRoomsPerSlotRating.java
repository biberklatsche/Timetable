package de.lars.wolfram.ttproblem.evolution.rating;

import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;

import java.util.HashSet;
import java.util.Set;

/**
 * @author wolframl
 */
public class PersonInDifferentRoomsPerSlotRating implements Rater
{

    public Double rate(IndividualPart individualPart)
    {
        Double rating = 0.0;
        for (TimeSlot time : individualPart.getTimeSlots())
        {
            Set<Person> differentPersons = new HashSet<>(individualPart.getPersons(time));
            rating += Math.abs(differentPersons.size() - individualPart.countRooms());
        }
        return rating;
    }
}
