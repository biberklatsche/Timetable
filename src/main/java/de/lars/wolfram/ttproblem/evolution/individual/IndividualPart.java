package de.lars.wolfram.ttproblem.evolution.individual;

import de.lars.wolfram.ttproblem.evolution.rating.Rater;
import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.TimeSlot;
import de.lars.wolfram.ttproblem.plan.timetable.TimetableKey;
import de.lars.wolfram.ttproblem.plan.timetable.TimetablePart;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * @author wolframl
 */
public class IndividualPart
{
    private TimetablePart timetablePart;
    private             Double                     rating            = Double.MAX_VALUE;
    public static final Comparator<IndividualPart> RATING_COMPARATOR = (o1, o2) -> o1.rating
            .compareTo(o2.rating);
    private final List<Rater> raters;

    IndividualPart(List<Rater> raters)
    {
        this.raters = raters;
    }

    public void changePerson(TimetableKey key, Person person)
    {
        this.timetablePart.setPerson(key, person);
    }

    public IndividualPart copy()
    {
        IndividualPart child = new IndividualPart(raters);
        child.rating = this.rating;
        child.timetablePart = this.timetablePart.copy();
        return child;
    }

    public int countRooms()
    {
        return timetablePart.countRooms();
    }

    public int countTimeSlots()
    {
        return timetablePart.countTimeSlots();
    }

    public Person getPerson(TimetableKey key)
    {
        return timetablePart.getPerson(key);
    }

    public List<Person> getPersons(Room room)
    {
        return timetablePart.getPersons(room);
    }

    public List<Person> getPersons(TimeSlot timeSlot)
    {
        return timetablePart.getPersons(timeSlot);
    }

    public Double getRating()
    {
        return rating;
    }

    public Set<Room> getRooms()
    {
        return timetablePart.getRooms();
    }

    public Set<TimeSlot> getTimeSlots()
    {
        return timetablePart.getTimeSlots();
    }

    public void rate()
    {
        rating = 0.0;
        for (Rater rater : raters)
        {
            rating += rater.rate(this);
        }
    }

    public void setPersons(List<Person> persons, Room room)
    {
        timetablePart.fillColumn(room, persons);
    }

    @Override public String toString()
    {
        return timetablePart.toString();
    }

    void setTimetablePart(TimetablePart timetablePart)
    {
        this.timetablePart = timetablePart;
    }
}
