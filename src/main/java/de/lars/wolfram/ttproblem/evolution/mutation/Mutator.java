package de.lars.wolfram.ttproblem.evolution.mutation;

import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;
import de.lars.wolfram.ttproblem.evolution.rand.Randomizer;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.timetable.TimetableKey;

/**
 * @author wolframl
 */
public class Mutator
{
    private final Randomizer randomGenerator;

    private final Double mutationRateNewPerson          = 0.1;
    private final Double mutationRatePersonFromSameRoom = 0.5;

    public Mutator(Randomizer randomGenerator)
    {
        this.randomGenerator = randomGenerator;
    }

    public void mutate(IndividualPart individualPart)
    {
        if (randomGenerator.nextDouble() <= mutationRateNewPerson)
        {
            TimetableKey key = randomGenerator.anyTimetableKey();
            Person person = randomGenerator.anyPerson();
            individualPart.changePerson(key, person);
        }
        else if (randomGenerator.nextDouble() <= mutationRatePersonFromSameRoom)
        {
            TimetableKey key = randomGenerator.anyTimetableKey();
            Person person = randomGenerator.anyPerson(individualPart.getPersons(key.getRoom()));
            individualPart.changePerson(key, person);
        }
    }

}
