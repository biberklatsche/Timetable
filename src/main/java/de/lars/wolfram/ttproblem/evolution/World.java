package de.lars.wolfram.ttproblem.evolution;

import de.lars.wolfram.ttproblem.evolution.crossover.Crossover;
import de.lars.wolfram.ttproblem.evolution.crossover.NormalCrossover;
import de.lars.wolfram.ttproblem.evolution.individual.IndividualPart;
import de.lars.wolfram.ttproblem.evolution.individual.IndividualPartBuilder;
import de.lars.wolfram.ttproblem.evolution.mutation.Mutator;
import de.lars.wolfram.ttproblem.evolution.rand.Randomizer;
import de.lars.wolfram.ttproblem.plan.building.Room;
import de.lars.wolfram.ttproblem.plan.persons.NaturalPerson;
import de.lars.wolfram.ttproblem.plan.persons.NoPerson;
import de.lars.wolfram.ttproblem.plan.persons.Person;
import de.lars.wolfram.ttproblem.plan.time.Period;
import de.lars.wolfram.ttproblem.plan.time.Quarter;
import de.lars.wolfram.ttproblem.plan.timetable.TimetableKey;
import de.lars.wolfram.ttproblem.plan.timetable.TimetablePartBuilder;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author wolframl
 */
public class World
{
    private final List<IndividualPart> individualParts;

    private final Mutator    mutator;
    private final Crossover  crossover;
    private final Randomizer randomizer;

    private final int countOfGenerations          = 20000;
    private final int countOfIndividuals          = 100;
    private final int countOfParentsPerGeneration = countOfIndividuals / 50;

    private final List<Person> personPool = Arrays
            .asList(new NaturalPerson("Klaus", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Peter", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Lars", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Rene", LocalTime.of(8, 0), LocalTime.of(16, 30)),
                    new NaturalPerson("Conny", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Susi", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Sascha", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Meik", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Marcel", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Roman", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Tobias", LocalTime.of(8, 0), LocalTime.of(16, 0)),
                    new NaturalPerson("Ronny", LocalTime.of(8, 0), LocalTime.of(16, 0)));

    private final List<Room> roomPool = Arrays
            .asList(new Room(1, "1"), new Room(2, "2"), new Room(3, "3"), new Room(4, "4"), new Room(5, "5"),
                    new Room(6, "6"), new Room(7, "7"), new Room(8, "8"), new Room(9, "9"),
                    new Room(10, "10"));

    public World()
    {
        List<Person> persons = getPersons(10);

        List<Room> rooms = getRooms(10);

        Period morningPeriod = new Period(LocalTime.of(9, 0), LocalTime.of(11, 45));

        TimetablePartBuilder morningBuilder = TimetablePartBuilder.newBuilder().period(morningPeriod)
                .rooms(rooms)
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 15)), roomPool.get(0)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 30)), roomPool.get(0)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 15)), roomPool.get(1)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 30)), roomPool.get(1)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 15)), roomPool.get(2)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 30)), roomPool.get(2)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 15)), roomPool.get(3)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 30)), roomPool.get(3)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 15)), roomPool.get(4)))
                .addMorningFreeRoom(new TimetableKey(new Quarter(LocalTime.of(11, 30)), roomPool.get(4)));

        randomizer = new Randomizer(persons, morningPeriod.getQuarters(), rooms);
        List<IndividualPart> individualParts = new ArrayList<>(100);
        IndividualPartBuilder builder = IndividualPartBuilder.newBuilder(randomizer).times(morningBuilder);
        for (int i = 0; i < countOfIndividuals; i++)
        {
            individualParts.add(builder.build());
        }
        Collections.sort(individualParts, IndividualPart.RATING_COMPARATOR);

        this.mutator = new Mutator(randomizer);
        this.crossover = new NormalCrossover(randomizer);
        this.individualParts = individualParts;
    }

    public IndividualPart getBestIndividual()
    {
        Collections.sort(individualParts, IndividualPart.RATING_COMPARATOR);
        return individualParts.get(0);
    }

    public void run() throws IOException
    {
        Collections.sort(individualParts, IndividualPart.RATING_COMPARATOR);
        double currentRating = Double.MAX_VALUE;
        for (int i = 0; i < countOfGenerations; i++)
        {
            List<IndividualPart> parents = getParents();
            individualParts.clear();
            for (int j = 0; j < countOfIndividuals; j++)
            {
                Integer indexParent1 = randomizer.nextInteger(parents.size());
                Integer indexParent2 = randomizer.nextInteger(parents.size());
                IndividualPart child = crossover.create(parents.get(indexParent1), parents.get(indexParent2));
                mutator.mutate(child);
                child.rate();
                individualParts.add(child);
            }
            Collections.sort(individualParts, IndividualPart.RATING_COMPARATOR);
            double newRating = getBestIndividual().getRating();
            if (newRating < currentRating)
            {
                currentRating = newRating;
                System.out.println("Generation:" + i + " Ranking:" + getBestIndividual().getRating());
            }
            if (getBestIndividual().getRating() == 0.0)
            {
                break;
            }
        }
        System.out.println(getBestIndividual() + "\n");
    }

    private List<IndividualPart> getParents()
    {
        List<IndividualPart> parents = new ArrayList<>(countOfParentsPerGeneration);
        parents.addAll(individualParts.subList(0, countOfParentsPerGeneration));
        return parents;
    }

    private List<Person> getPersons(int size)
    {
        List<Person> persons = new ArrayList<>(personPool.subList(0, size));
        persons.add(new NoPerson());
        return persons;
    }

    private List<Room> getRooms(int size)
    {
        return roomPool.subList(0, size);
    }
}
